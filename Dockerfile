FROM repsistance/cardano-node:iohk-mn-passive-1.27.0-0

VOLUME ["/data"]

RUN apt-get update -qy && \
    apt-get install -y postgresql-client && \
    apt-get clean

ADD ./assets /assets
RUN chown -R nobody: /assets
WORKDIR /assets/bin

ENTRYPOINT ["/bin/bash", "-c", "chown -R nobody: /data ${CARDANO_NODE_SOCKET_PATH}; sudo -EHu nobody /assets/bin/entrypoint"]
